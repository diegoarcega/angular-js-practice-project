'use strict';

/**
 * @ngdoc function
 * @name swbApp.controller:chooseUniversityController
 * @description
 * # chooseUniversityController
 * Controller of the swbApp
 */

function chooseUniversityController($rootScope, $scope, UniversityFactory, splitFilter){

	$scope.modal = { name: 'modal-login-post', url: 'views/partials/modal-login-post.html'};

	$rootScope.myRef = new Firebase("https://luminous-torch-6350.firebaseio.com");
  	$rootScope.authClient = new FirebaseSimpleLogin($rootScope.myRef, function(error, user) {
	     if (error) {
	      	// an error occurred while attempting login
	     	console.log(error);
	     }else if (user) {
	      	// user authenticated with Firebase
	     	$rootScope.user = user;
	     	$rootScope.loadUser();
	    	} else {
	      // user is logged out
	    	}
	});


}



chooseUniversityController.$inject = ['$rootScope','$scope', 'UniversityFactory', 'splitFilter'];

angular
	.module('swbApp')
	.controller('chooseUniversityController', chooseUniversityController);


