'use strict';

angular
.module('swbApp')
.directive('indexDirective', function(){
	return {
		templateUrl: 'views/index.html',
		controller:'indexController'
	}
});