'use strict';

/**
 * @ngdoc function
 * @name swbApp.controller:indexController
 * @description
 * # indexController
 * Controller of the swbApp, index page
 */
angular
.module('swbApp')
.controller('indexController', ['$rootScope', '$scope', '$firebase', 'UniversityFactory', function ($rootScope, $scope, $firebase, UniversityFactory) {

  $scope.modal = { name: 'modal-login-post', url: 'views/partials/modal-login-post.html'} ;

  $scope.init = function () {

  $( "#universities-list" ).each(function( index ) {
      if($(this).attr('exists')=="false") {
            $(this).listnav({
               initLetter: '',        // filter the list to a specific letter on init ('a'-'z', '-' [numbers 0-9], '_' [other])
              includeAll: true,      // Include the ALL button
              includeOther: false,    // Include a '...' option to filter non-english characters by
              includeNums: false,     // Include a '0-9' option to filter by
              flagDisabled: true,    // Add a class of 'ln-disabled' to nav items with no content to show
              removeDisabled: true, // Remove those 'ln-disabled' nav items (flagDisabled must be set to true for this to function)
              noMatchText: 'No matching entries', // set custom text for nav items with no content to show
              showCounts: false,      // Show the number of list items that match that letter above the mouse
              cookieName: null,      // Set this to a string to remember the last clicked navigation item requires jQuery Cookie Plugin ('myCookieName')
              onClick: function(letter){ null; },         // Set a function that fires when you click a nav item. see Demo 5
              prefixes: [],          // Set an array of prefixes that should be counted for the prefix and the first word after the prefix ex: ['the', 'a', 'my']
              filterSelector: '.last-name'     // Set the filter to a CSS se
            });
      $(this).attr('exists','true');

      }
  });
};

  //Set the carousel options
  $('#quote-carousel').carousel({
    pause: true,
    interval: 4000,
  });

  $('#media').carousel({
    pause: true,
    interval: false,
  });

  // On Document Ready Function
  $scope.$watch('$viewContentLoaded', function()
          {
            setTimeout(function(){
              $scope.init();
              },50);
  });

  $rootScope.myRef = new Firebase("https://luminous-torch-6350.firebaseio.com");
  $rootScope.authClient = new FirebaseSimpleLogin($rootScope.myRef, function(error, user) {
    if (error) {
      // an error occurred while attempting login
      console.log(error);
    } else if (user) {
      // user authenticated with Firebase
      $rootScope.user = user;
      $rootScope.loadUser();
    } else {
      // user is logged out
    }
  });

}]);
