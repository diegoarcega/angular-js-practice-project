'use strict';
// using firebase database in json


function firebaseController($firebase){

	return{
			 var ref = new Firebase('https://luminous-torch-6350.firebaseio.com/');
			  // ref.push({
			  //   accommodations: { id:"2", id_university: "20", name:"Vine Court"}
			  // });
			  var sync = $firebase(ref);
			  // Create the tables ACCOMMODATIONS and UNIVERSITIES like in MYSQL TABLES
			  sync.$set({
			    accommodations: [
			                        {
			                          id: "1",
			                          id_university: "20",
			                          name: "Vine Court"
			                        },
			                        {
			                          id: "2",
			                          id_university: "20",
			                          name: "Carnatic Halls"
			                        }
			                    ],
			    universities: [
			                       {
			                          id: "1",
			                          name: "University of Liverpool"
			                        },
			                        {
			                          id: "2",
			                          name: "University of Leeds"
			                        }
			                  ]
			    });
			  // var syncObject = sync.$asObject();
			  // syncObject.$bindTo($scope, "dado");
			  // $scope.dado = syncObject;
			  // console.debug($scope.dado);
	};
}

angular
	.module('swbApp')
	.controller('firebaseController', firebaseController);