'use strict';

/**
 * @ngdoc function
 * @name swbApp.controller:modalController
 * @description
 * # modalController
 * Controller of the swbApp, modal for login-post
 */
angular
  .module('swbApp')
  .controller('modalController', ['$rootScope','$scope', '$http', 'CommentFactory', 'AccommodationFactory',
     function ($rootScope, $scope, $http, CommentFactory, AccommodationFactory) {



     $scope.postUniversity = function(){
        if ($rootScope.universityCommentsOBJ === undefined) {
          var id = 0;
          $rootScope.universityCommentsOBJ = [{}];
        }else{
          var id = (parseInt($rootScope.universityCommentsOBJ[$rootScope.universityCommentsOBJ.length-1].id) + 1);
        }
        var newComment = { id: $scope.university.id, university: $scope.university, comment: $scope.comment_university };
        $rootScope.universityCommentsOBJ.push(newComment);

        CommentFactory.processUniversityForm($scope.university, $scope.comment_university).then(function(fullResponse){
           alert('posted with success!');
        });
     };

    $scope.postAccommodation = function(university, comment_accommodation, accommodation){
        if ($rootScope.accommodationCommentsOBJ[$rootScope.accommodationCommentsOBJ.length-1] === undefined) {
          var id = 0;
        }else{
          var id = (parseInt($rootScope.accommodationCommentsOBJ[$rootScope.accommodationCommentsOBJ.length-1].id) + 1);
        }
        var newComment = { id: id, university: $rootScope.accommodation, comment: $scope.comment_accommodation };
        $rootScope.accommodationCommentsOBJ.push(newComment);

        CommentFactory.processAccommodationForm($scope.university, $scope.comment_accommodation, $scope.accommodation.id)
        .then(function(fullResponse){      },function(error){console.error(error); });
     };


    $scope.getCommentsByAccommodation = function(){
      if ($scope.accommodation != undefined && $scope.university != undefined) {
          CommentFactory.getAccommodation($scope.university.id, $scope.accommodation.id).then(function(dataDB){
          $rootScope.accommodationCommentsOBJ = dataDB.data;
         },function(error){console.error(error);});
      }
    };

   // LOGIN START

  $rootScope.loadUser = function() {
      if ($('.logout').css("visibility") != "visible" && $('#user-photo') != undefined ) {
        $('.user-logged-div').append("<img src='"+ $rootScope.user.thirdPartyUserData.picture + "' class='user-quote img-circle' id='user-photo'>");
        $('.user-logged-name').prepend('<span class="text-left">'+ $rootScope.user.displayName + ' | </span>');
        $('.user-name').text($rootScope.user.displayName);
        $('.modal-comment-photo').attr("src", $rootScope.user.thirdPartyUserData.picture);
        $('.logout').css("visibility", 'visible');
        setTimeout(function(){
          $('#login-modal').modal('hide');
        },500);
      }
  };
  $scope.logOut = function(){
        $('.user-logged-div img').remove();
        $('.logout').css("visibility", 'hidden');
        $('.user-logged-name').text('');
        $('.user-name').text('Login in');
        $('.modal-comment-photo').attr("src","images/notloggedin.png");
        $rootScope.user = "";
        $rootScope.authClient.logout();
  };
  $scope.login = function(){
        $rootScope.authClient.login("google",{
          rememberMe: true,
          scope: 'https://www.googleapis.com/auth/plus.login'
        });
  };

   // LOGIN END


//  FACEBOOK LOGIN API
/*
     $scope.login = function (){
        FB.login(function(response) {
          $scope.loadUser();
          setInterval(function(){
             $('.btn-facebook').text('You are now logged in as ' + $scope.user.name);
           },500);

        });
      };


    $scope.loadUser = function() {
            FB.api('/me', function(response) {
                $('.user-logged-div').append("<img src='https://graph.facebook.com/"+ response.id +"/picture' class='user-quote img-circle'>");
                $('.user-logged-name').prepend('<span class="text-left">'+ response.name + ' | </span>');
                $('.user-name').text(response.name);
                $('.modal-comment-photo').attr("src","https://graph.facebook.com/"+ response.id +"/picture?width=115&height=115");
                $('.logout').css("visibility", 'visible');
                //console.log(JSON.stringify(response));
                $scope.user = response;
                setTimeout(function(){
                  $('#login-modal').modal('hide');
                },2400);
            });
    };



    $scope.logOut = function(){
        $('.user-logged-div img, .logout').remove();
        $('.user-logged-name').text('');
        FB.logout(function(response) {
                $('.user-name').text('Login in');
                $('.modal-comment-photo').attr("src","../images/notloggedin.png");
      });
        $scope.user = "";
    };


     window.fbAsyncInit = function() {
          FB.init({
            appId      : '1461589710759107',
            cookie     : true,  // enable cookies to allow the server to access
                                // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.0' // use version 2.0
          });

          FB.getLoginStatus(function(response) {
               if (response.status === 'connected') {
                $scope.loadUser();
              } else if (response.status === 'not_authorized') {
                console.log('disconnected');
              } else {
                console.log('disconnected');
              }
          });

      };

       (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));


        $("#accommodation-photo").fileinput({
          previewFileType: "image",
          browseClass: "btn btn-success",
          browseLabel: "Pick Image",
          browseIcon: '<i class="glyphicon glyphicon-picture"></i>',
          removeClass: "btn btn-danger",
          removeLabel: "Delete",
          removeIcon: '<i class="glyphicon glyphicon-trash"></i>',
          uploadClass: "btn btn-info",
          uploadLabel: "Upload",
          uploadIcon: '<i class="glyphicon glyphicon-upload"></i>',
        });

       // $('[name="upload-photo-modal"], [name="university-modal"], [name="accomodation-modal"]').click(function(event) {
       //      var _self = this;
       //      $scope.isLogged(_self);
       //  });

    // $scope.self;
    // $scope.isLogged = function(_self) {
        // $scope.self = _self;
        // FB.getLoginStatus(function(response) {
        //      if (response.status === 'connected') {
              // $('#'+ $scope.self.name).modal('show');
        //     } else if (response.status === 'not_authorized') {
        //       $('#login-modal').modal('show');
        //     } else {
        //       $('#login-modal').modal('show');
        //     }
        // });
      // };

*/

}]);
