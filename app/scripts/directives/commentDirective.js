
function commentDirective($rootScope){


	// Loading event listeners
	var _self;

	function isLogged(_self){
      // console.debug($rootScope.user);
      this.self = _self;
      var authRef = new Firebase("https://luminous-torch-6350.firebaseio.com/.info/authenticated");
      authRef.on("value", function(snap) {
        if (snap.val() === true) {
          console.log("authenticated");
          $('#'+ this.self.name).modal('show');
        } else {
          console.debug("not authenticated");
          $('#login-modal').modal('show');
        }
      });
	}

	function loadEventListeners(){
       $('[name="upload-photo-modal"], [name="university-modal"], [name="accomodation-modal"]').click(function(event) {
            _self = this;
            isLogged(_self);
        });
 	}



	return{
		returning :  loadEventListeners()
	};
}

commentDirective.$inject = ['$rootScope'];

angular
	.module('swbApp')
	.directive('commentDirective', commentDirective);
