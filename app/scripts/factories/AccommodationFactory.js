'use strict';

// Minification OK!


function accommodationFactory($http) {

    return {

        getAllByUniversityID: function(id_university) {

            var httpPromise = $http.post('server/controllers/post-comment.php', {
                id_university: id_university,
                type: "getAllByUniversityID"
            });

            return httpPromise;
        },

        getAll: 'none',

        getOneByID: ''
    }
}

accommodationFactory.$inject = ['$http'];

angular
    .module('swbApp')
    .factory('AccommodationFactory', accommodationFactory);