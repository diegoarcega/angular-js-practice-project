<?php
require_once "../classes/Db.php";
$data = file_get_contents("php://input");
$request = json_decode($data);

$db = new DB();
$db->connect();

switch ($request->type) {
	case 'getAllByUniversityID':
		$sql = "SELECT * FROM accommodations WHERE id_university='$request->id_university' ";
		$result = $db->query($sql);
		echo json_encode($result, JSON_NUMERIC_CHECK);
		break;

	case 'getUniversity':
		$sql = "SELECT * FROM comments WHERE id_accommodation = '' AND id_university='$request->id_university'";
		$result = $db->query($sql);
		echo json_encode($result, JSON_NUMERIC_CHECK);
		break;

	case 'getAccommodation':
		$sql = "SELECT * FROM comments WHERE id_accommodation = '$request->id_accommodation' AND id_university='$request->id_university' ";
		$result = $db->query($sql);
		echo json_encode($result, JSON_NUMERIC_CHECK);
		break;

	case 'processUniversityForm':
		$id_university = $request->university->id;

		$sql = "INSERT INTO comments(comment,id_university, id_user) VALUES('$request->comment_university','$id_university','1')";
		$db->query($sql);
		break;

	case 'processAccommodationForm':
		$id_university = $request->university->id;
		$id_accommodation = $request->id_accommodation;

		$sql = "INSERT INTO comments(comment,id_university, id_accommodation, id_user) VALUES('$request->comment_university','$id_university','$id_accommodation','1')";
		$db->query($sql);
		break;

	case 'getAllUniversities':
		$sql = "SELECT * FROM universities";
		$result = $db->query($sql);
		echo json_encode($result, JSON_NUMERIC_CHECK);
		break;


	default:
		# code...
		break;
}

?>