'use strict';


function universityProfileController($firebase, $location, $routeParams, $rootScope, $scope, CommentFactory, AccommodationFactory){
   $scope.modal = { name: 'modal-login-post', url: 'views/partials/modal-login-post.html'};

   if($routeParams.id_university== undefined) { $location.path("/"); return; }

   var id_choosen_university = $routeParams.id_university;
   $rootScope.id_choosen_university = id_choosen_university;

  // Loading choosen university inside the select option input forms AS A SELECTED ITEM, whatever is in ng-model of the <select> is considered a selected option
    angular.forEach($rootScope.allUniversities, function(value,key){
        if(value.id == $rootScope.id_choosen_university){
            $scope.university =  $rootScope.allUniversities[key];
        }
     });

    AccommodationFactory.getAllByUniversityID($rootScope.id_choosen_university).then(function(dataDB){
      $rootScope.accommodations = dataDB.data;
      $scope.selectAccommodation.accommodation = $rootScope.accommodations[0];
    },function(error){console.error(error);});


   CommentFactory.getUniversity(id_choosen_university).then(function(dataDB){
    $rootScope.universityCommentsOBJ = dataDB.data;
   },function(error){console.error(error);});

  CommentFactory.getAccommodation(id_choosen_university,"1").then(function(dataDB){
    $rootScope.accommodationCommentsOBJ = dataDB.data;
   },function(error){console.error(error);});

  AccommodationFactory.getAllByUniversityID(id_choosen_university).then(function(dataDB){
    $scope.accommodations = dataDB.data;
  }, function(error){console.error(error);});

  $scope.getCommentsByAccommodation = function(){
    CommentFactory.getAccommodation(id_choosen_university, $scope.accommodation.id).then(function(dataDB){
    $rootScope.accommodationCommentsOBJ = dataDB.data;
   },function(error){console.error(error);}); };


  $rootScope.myRef = new Firebase("https://luminous-torch-6350.firebaseio.com");
  $rootScope.authClient = new FirebaseSimpleLogin($rootScope.myRef, function(error, user) {
    if (error) {
      // an error occurred while attempting login
      console.log(error);
    } else if (user) {
      // user authenticated with Firebase
      $rootScope.user = user;
      $rootScope.loadUser();
    } else {
      // user is logged out
    }
  });
  // console.debug($rootScope.user);

}

universityProfileController.$inject = ['$firebase','$location','$routeParams','$rootScope','$scope','CommentFactory', 'AccommodationFactory' ] ;

angular
  .module('swbApp')
  .controller('universityProfileController', universityProfileController);
