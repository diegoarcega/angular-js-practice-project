'use strict';


angular
	.module('swbApp')
	.factory('CommentFactory',['$timeout','$rootScope', '$http', '$q',
		function($timeout, $rootScope, $http, $q){

		 return{
			    processUniversityForm : function(university, comment_university){

			    var httpPromise = $http.post('server/controllers/post-comment.php', {
			      university: university,
			      comment_university: comment_university,
			      type:"processUniversityForm"
			    });

			    return httpPromise;

			  },

			  processAccommodationForm : function(university, comment_accommodation, id_accommodation){

			    var httpPromise = $http.post('server/controllers/post-comment.php', {
			      university: university,
			      comment_university: comment_accommodation,
			      id_accommodation: id_accommodation,
			      type: "processAccommodationForm"
			    });

			    return httpPromise;

			  },
			  getUniversity : function(id_university){

			    var httpPromise = $http.post('server/controllers/post-comment.php', {
			      id_university: id_university,
			      type: "getUniversity"
			    });

			    return httpPromise;

			  },
			  getAccommodation : function(id_university, id_accommodation){

			    var httpPromise = $http.post('server/controllers/post-comment.php', {
			      id_university: id_university,
			      id_accommodation: id_accommodation,
			      type: "getAccommodation"
			    });

			    return httpPromise;

			  }




		};

	}]);