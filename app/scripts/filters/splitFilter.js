function splitFilter() {
	     return function(input, delimiter) {
		     var delimiter = delimiter || ' ';
		     if (input != undefined) {
		     	for (var i = input.length - 1; i >= 0; i--) {
		     		input[i].lastName = input[i].name.split(delimiter);
		     	};
		     }
		     return input;
    		}

}

// splitFilter.$inject = ['$rootScope'];

angular
	.module('swbApp')
	.filter('split', splitFilter);



