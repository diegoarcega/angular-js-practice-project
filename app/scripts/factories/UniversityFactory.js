'use strict';

function universityFactory($http) {
    return {

        getAll: function() {
            var httpPromise = $http.post('server/controllers/post-comment.php', {
                type: "getAllUniversities"
            });

            return httpPromise;
        }

    };
}

universityFactory.$inject = ['$http'];

angular
    .module('swbApp')
    .factory('UniversityFactory', universityFactory);