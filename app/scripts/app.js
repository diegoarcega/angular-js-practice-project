'use strict';

/**
 * @ngdoc overview
 * @name swbApp
 * @description
 * # swbApp
 *
 * Main module of the application.
 */

var swb = angular
    .module('swbApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'firebase'
    ]);


angular
    .module('swbApp')
    .run(function($rootScope, UniversityFactory) {
        // Loading all the Universities from the Database inside a variable in the global scope(rootScope)
        UniversityFactory.getAll().then(function(dataDB){
           $rootScope.allUniversities = dataDB.data;
        },function(error){console.error(error)});

        $rootScope.islogged = function isLogged(obj, $event){
              var authRef = new Firebase("https://luminous-torch-6350.firebaseio.com/.info/authenticated");
              authRef.on("value", function(snap) {
                if (snap.val() === true) {
                  console.log("authenticated");
                  $('#'+ $event.target.name).modal('show');
                } else {
                  console.debug("not authenticated");
                  $('#login-modal').modal('show');
                }
              });
        }
    });

angular
    .module('swbApp')
    .config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/templates/index.tpl.html',
        })
        .when('/stepbystep', {
            templateUrl: 'views/stepbystep.html'
        })
        .when('/choose_university', {
            templateUrl: 'views/choose_university.html',
            controller: 'chooseUniversityController'
        })
        .when('/forum', {
            templateUrl: 'views/index.html'
        })
        .when('/videos', {
            templateUrl: 'views/videos.html'
        })
        .when('/documents', {
            templateUrl: 'views/documents.html'
        })
        .when('/externallinks', {
            templateUrl: 'views/externallinks.html'
        })
        .when('/stepbystep2', {
            templateUrl: 'views/stepbystep2.html'
        })
        .when('/stepbystep3', {
            templateUrl: 'views/stepbystep3.html'
        })
        .when('/university_profile/:id_university', {
            templateUrl: 'views/university_profile.html',
            controller: 'universityProfileController'
        })
        .otherwise({
            redirectTo: '/'
        });

    // configure html5 to get links working
    // If you don't do this, you URLs will be base.com/#/home rather than base.com/home
    // $locationProvider.html5Mode(true);
});